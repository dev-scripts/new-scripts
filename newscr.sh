#!/bin/bash

# The purpose of this script is to create a new script, make it exicutible, and open it in vim
read -p "Press 1 for a new shell script and 2 for a new python script: " choice

read -p "Enter new script name (without extension): " name

if [ $choice -eq 1 ]
then
	scr=".sh"
	echo "#! /usr/bin/bash" > $name$scr && chmod +x $name$scr && vi $name$scr

elif [ $choice -eq 2 ]
then
	scr=".py"
	echo "#! /usr/bin/env python3" > $name$scr && chmod +x $name$scr && vi $name$scr
fi


